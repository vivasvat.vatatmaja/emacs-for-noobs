;; init.el file: THIS FILE IS YOUR CONFIGURATION FOR EMACS

;;; Code:
; Package Initialization: --------------------------------

;; `require' is a built in elisp function that essentially loads a library. The difference between `require' and `load' is that when `require' is called multiple times on the same library, it provides the optimization of only loading it once.
(require 'package)

;; setq is a special form in elisp. For our purposes pretend it is a function that equates the `package-selected-packages' variable to a list of packages. For now there is only 1 element in our list: doom-themes: which is a selection of themes. Feel free to edit this line and add even more packages by simply entering them into the package list. This variable will be used by package.el to automatically fetch certain packages you may want to install. Note that you will have to reload emacs for this list of packages to be fetched and loaded by packgae.el.
(setq package-selected-packages '(doom-themes
				  doom-modeline
				  ivy
				  ;; package-X
				  ;; package-Y
				  ;; package-Z
				  )
;; Alternatively, you may manually install a package by doing M-x package-install RET package-X RET


;; Here we are adding the melpa package repository to our package archives to expand the selection of packages available for you to install.
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/"))


;; EXPLANATION TODO
(package-initialize)

;; EXPLANATION TODO
(unless package-archive-contents
  (package-refresh-contents))
(package-install-selected-packages)

;;;; File Settings: ------------------------------------------
;; This variable controls the buffer that is initially loaded by emacs. Change this to get a different 'Welcome' sceen.
(setq initial-buffer-choice "~/.emacs.d/init.el" )

;; It is good practice to have configurations for different packages in different files. This allows for clean configuratoin management. All of the configuration we have for specific files will be in ~/.emacs.d/init/. Checkout the example configuration file ~/.emacs.d/init/example.el.
(add-to-list 'load-path "~/.emacs.d/init/")

;; Place any lisp functions YOU make inside this directory.
(add-to-list 'load-path "~/.emacs.d/site-lisp/")

;; Place any emacs themes YOU make inside this directory.
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

;; There are special variables in Emacs called custom variables. that you can access with M-x customize. These variables are automatically concated to your init file but it gets clutters so it is common practice to move these custom variables to a seperate file where they can do their own thing.
(setq custom-file "~/.emacs.d/.custom.el")
(load custom-file)

;; EXPLANATION TODO
(setq backup-directory-alist '(("." . "~/.emacs.d/.backups")))
(setq backup-by-copying t)

(setq delete-old-versions t
      kept-new-versions 20
      kept-old-versions 10
      version-control t)

;; EXPLANATION TODO
(require 'org-bullets)

(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;; EXPLANTION TODO
(load-theme doom-solarized-light)
(doom-modeline-mode)
(ivy-mode)

(provide 'init)
;;; init.el ends here
